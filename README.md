# Flipper Zero Scripts

## Summary
This is a collection of files and scripts that are created for the popular [Flipper Zero](https://flipperzero.one/). Core features and functionality of the Flipper Zero can be found [in their documentation repository](https://docs.flipperzero.one/). This repository is broken down in to the following sections which are detailed below.

### BadUSB
These are [Ducky Script 1.0 compatible](https://web.archive.org/web/20220816200129/http://github.com/hak5darren/USB-Rubber-Ducky/wiki/Duckyscript) scripts that will run on the Flipper Zero. Note that specific script documentation has been included in each script.

### Music
The Flipper Zero default music player will accept FMF files (or Flipper Music Files). While not 100% comprehensive, [this repository created by Tonsil](https://github.com/Tonsil/flipper-music-files) does explain a fair amount of what you need to know. The rest of it is just music theory. :D

## More Sections To Come
There will be more coming as I learn more about the Flipper Zero so stay tuned!